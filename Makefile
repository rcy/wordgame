start:
	yarn dev

install:
	yarn

words: pages/api/words.json

/tmp/words.txt: /usr/share/dict/american-english /usr/share/dict/british-english personal
	sort $^ | uniq > $@

personal:
	touch $@

%/words.json: /tmp/words.txt
	echo "[" > $@
	gawk '{ print "\"" $$1 "\"," }' < $< >> $@
	echo "null ]" >> $@
