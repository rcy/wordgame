import createPersistedState from 'use-persisted-state';

const useAwardsHistoryState = createPersistedState('awards')
const useAwardsTimestampHistoryState = createPersistedState('awards-timestamp')
const useWordsHistoryState = createPersistedState('history')

export {
  useAwardsHistoryState,
  useAwardsTimestampHistoryState,
  useWordsHistoryState,
}
