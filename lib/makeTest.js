// Return array of arrays of COUNT addends with none being greater that MAX that add to TOTAL.
function summations({ total, max = total, count = 2 }) {
  const result = []

  for (let c = 0; c <= total; c++) {
    for (let b = 0; b <= Math.min(max, c); b++) {
      result.push([c - b, b])
    }
  }

  return result
}

function sumArrToQa(x) {
  return { q: `${x[0]}+${x[1]}=`, a: `${x[0]+x[1]}` }
}

function subtractions({ total, max = total, count = 2 }) {
  const result = []

  for (let c = 0; c <= total; c++) {
    for (let b = 0; b <= c; b++) {
      result.push([c, b])
    }
  }

  return result
}

function subArrToQa(x) {
  return { q: `${x[0]}-${x[1]}=`, a: `${x[0]-x[1]}` }
}

function addId(x) {
  return {...x, id: Math.random() }
}

export const tests = [
  {
    name: 'add1',
    title: 'Addition 1',
    description: 'Add numbers up to 5',
    color: 'orange',
    emoji: '🐱', // https://emojipedia.org/cat-face
    questions: summations({ total: 5, max: 5 }).map(sumArrToQa).map(addId),
  },
  {
    name: 'add2',
    title: 'Addition 2',
    description: 'Add numbers up to 10',
    color: 'purple',
    questions: summations({ total: 10, max: 10 }).map(sumArrToQa).map(addId),
    emoji: '🦆', // https://emojipedia.org/duck/
  },
  {
    name: 'add3',
    title: 'Addition 3',
    description: 'Add numbers up to 12',
    color: 'gray',
    questions: summations({ total: 12, max: 12 }).map(sumArrToQa).map(addId),
    emoji: '🐺', // https://emojipedia.org/wolf/
  },
  {
    name: 'add4',
    title: 'Addition 4',
    description: 'Add numbers up to 15',
    color: 'green',
    questions: summations({ total: 15, max: 15 }).map(sumArrToQa).map(addId),
    emoji: '🐸', // https://emojipedia.org/frog/
  },
  {
    name: 'add5',
    title: 'Addition 5',
    description: 'Add numbers up to 20',
    color: 'brown',
    questions: summations({ total: 20, max: 20 }).map(sumArrToQa).map(addId),
    emoji: '🐻', // https://emojipedia.org/bear/
  },
  {
    name: 'sub1',
    title: 'Subtraction 1',
    description: 'Subtract numbers up to 5',
    color: '#FFBDBD',
    questions: subtractions({ total: 5 }).map(subArrToQa).map(addId),
    emoji: '🐭', // https://emojipedia.org/mouse-face/
  },
  {
    name: 'sub2',
    title: 'Subtraction 2',
    description: 'Subtract numbers up to 10',
    color: '#F65A1F',
    questions: subtractions({ total: 10 }).map(subArrToQa).map(addId),
    emoji: '🦊', // https://emojipedia.org/fox/
  },
  {
    name: 'cap1',
    title: 'Capitals 1',
    description: 'Capitals of Canadian Provinces and Territories',
    color: 'black',
    questions: [
      {	q: 'British Columbia', a: 'Victoria', },
      {	q: 'Alberta', a: 'Edmonton', },
      { q: 'Saskatchewan', a: 'Regina', },
      { q: 'Manitoba', a: 'Winnipeg', },
      { q: 'Ontario', a: 'Toronto', },
      { q: 'Prince Edward Island', a: 'Charlottetown' },
      { q: 'Quebec', a: 'Quebec City', },
      { q: 'New Brunswick', a: 'Fredericton', },
      { q: 'Nunavut', a: 'Iqaluit', },
      { q: 'Northwest Territories', a: 'Yellowknife', },
      { q: 'Newfoundland and Labrador', a: "St. John's", },
      { q: 'Nova Scotia', a: 'Halifax' },
      { q: 'Yukon', a: 'Whitehorse', },
    ].map(addId),
    emoji: '🇨🇦', // https://emojipedia.org/flag-canada
  },
  {
    name: 'roman1',
    title: 'Roman Numerals I',
    description: '1-5 / I-V',
    color: 'black',
    emoji: '🦌', // https://emojipedia.org/deer/
    questions: [
      { q: '1', a: 'I', },
      { q: '2', a: 'II', },
      { q: '3', a: 'III', },
      { q: '4', a: 'IV', },
      { q: '5', a: 'V', },
      { q: 'I', a: '1', },
      { q: 'II', a: '2', },
      { q: 'III', a: '3', },
      { q: 'IV', a: '4', },
      { q: 'V', a: '5', },
    ].map(addId)
  },
  {
    name: 'roman2',
    title: 'Roman Numerals II',
    description: '5-10 / V-X',
    color: 'blue',
    emoji: '🦓', // https://emojipedia.org/zebra/
    questions: [
      { q: '5', a: 'V', },
      { q: '6', a: 'VI', },
      { q: '7', a: 'VII', },
      { q: '8', a: 'VIII', },
      { q: '9', a: 'IX', },
      { q: '10', a: 'X', },
      { q: 'V', a: '5', },
      { q: 'VI', a: '6', },
      { q: 'VII', a: '7', },
      { q: 'VIII', a: '8', },
      { q: 'IX', a: '9', },
      { q: 'X', a: '10', },
    ].map(addId)
  },
  {
    name: 'roman3',
    title: 'Roman Numerals III',
    description: '10-20 / X-XX',
    color: '#eea2ad',
    emoji: '🐷', // https://emojipedia.org/pig-face/
    questions: [
      { q: '10', a: 'X', },
      { q: '11', a: 'XI', },
      { q: '12', a: 'XII', },
      { q: '13', a: 'XIII', },
      { q: '14', a: 'XIV', },
      { q: '15', a: 'XV', },
      { q: '16', a: 'XVI', },
      { q: '17', a: 'XVII', },
      { q: '18', a: 'XVIII', },
      { q: '19', a: 'XIX', },
      { q: '20', a: 'XX', },
      { q: 'X', a: '10', },
      { q: 'XI', a: '11', },
      { q: 'XII', a: '12', },
      { q: 'XIII', a: '13', },
      { q: 'XIV', a: '14', },
      { q: 'XV', a: '15', },
      { q: 'XVI', a: '16', },
      { q: 'XVII', a: '17', },
      { q: 'XVIII', a: '18', },
      { q: 'XIX', a: '19', },
      { q: 'XX', a: '20', },
    ].map(addId)
  },
  {
    name: 'opp1',
    title: 'Opposites 1',
    description: 'Opposite Words',
    color: 'cyan',
    emoji: '🐟', // https://emojipedia.org/fish/
    questions: [
      ['big', 'small'],
      ['near', 'far'],
      ['short', 'tall'],
      ['open', 'closed'],
      ['loud', 'quiet'],
      ['dark', 'light'],
      ['happy', 'sad'],
    ].map(x => ({ q: x[0], a: x[1] })).map(addId)
  },
  {
    name: 'opp2',
    title: 'Opposites 2',
    description: 'Opposite Words',
    color: 'salmon',
    emoji: '🐙', // https://emojipedia.org/octopus
    questions: [
      ['wet', 'dry'],
      ['hot', 'cold'],
      ['empty', 'full'],
      ['thick', 'thin'],
      ['dangerous', 'safe'],
      ['black', 'white'],
      ['new', 'old'],// young
      ['first', 'last'],
      ['wide', 'narrow'],
      ['up', 'down']
    ].map(x => ({ q: x[0], a: x[1] })).map(addId)
  },
  {
    name: 'blank1',
    title: 'Fill In The Blanks 1',
    description: 'What is the missing letter?',
    emoji: '🦄', // unicorn
    color: 'purple',
    questions: [
      { q: 'm_mmy', a: 'o' },
      { q: 'd_ddy', a: 'a' },
      { q: 'h_use', a: 'o' },
      { q: 'b_nny', a: 'u' },
      { q: 'k_tty', a: 'i' },
      { q: 'm_use', a: 'o' },
      { q: 'u_icorn', a: 'n' },
      { q: 'b_eakfast', a: 'r' },
      { q: 'l_nch', a: 'u' },
      { q: 'd_nner', a: 'i' },
    ].map(addId)
  }
]
