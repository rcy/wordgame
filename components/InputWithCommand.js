import { useRouter } from 'next/router';
import { useState, useEffect } from 'react'
import { tests } from '../lib/makeTest.js'
import { Input } from './Input'

export const InputWithCommand = ({ onSubmit = () => {}, onChange = () => {}, placeholder, ...props }) => {
  const router = useRouter()
  const [commands, setCommands] = useState([])
  const [error, setError] = useState()
  const [completions, setCompletions] = useState([])
  const [commandMode, setCommandMode] = useState(false)

  useEffect(() => {
    const commands = tests.map(t => ({ name: t.name, route: `/quiz?test=${t.name}`, emoji: t.emoji }))
    commands.push({ name: 'words', route: '/', emoji: '📚' })
    commands.push({ name: 'artist', route: '/artist', emoji: '🎨' })
    setCommands(commands)
  }, [router])

  function handleCommand(value) {
    const command = commands.find(cmd => cmd.name === value)
    if (command) {
      if (command.route) {
        router.push(command.route)
      }
    } else {
      setError(`no ${value}`)
    }
  }

  function handleSubmit(value) {
    if (value[0] === '/') {
      handleCommand(value.slice(1))
    } else {
      onSubmit(value)
    }
  }

  function handleChange(value) {
    setError(null)
    if (value[0] === '/') {
      const partial = value.slice(1)
      setCompletions(commands.filter(cmd => cmd.name.substring(0, partial.length) === partial))
      setCommandMode(true)
    } else {
      setCompletions([])
      setCommandMode(false)
    }
    onChange(value)
  }

  return (
    <div>
      <Input
        {...props}
        onSubmit={handleSubmit}
        onChange={handleChange}
        style={{
          background: commandMode ? 'black' : 'inherit',
          color: commandMode ? 'white' : 'inherit'
        }}
        placeholder={placeholder || "/command"}
      />

      {commandMode && (
        <div className="completions" >
          {completions.map(m => (
            <div className="item" key={m.name}>
              {m.emoji}/{m.name}&nbsp;&nbsp;&nbsp;
            </div>
          ))}
        </div>
      )}
    </div>
  )
}
