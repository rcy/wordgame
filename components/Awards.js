import { useEffect, useState } from 'react'
import { useAwardsHistoryState, useAwardsTimestampHistoryState } from '../lib/persistence'

const MINUTES_TO_KEEP = 60 * 8;

export function Awards() {
  const [tick, setTick] = useState(0)
  const [awards, setAwards] = useAwardsHistoryState([])
  const [awardsTimestamp, setAwardsTimestamp] = useAwardsTimestampHistoryState(0)

  useEffect(() => {
    console.log('check')
    if (awardsTimestamp && ((Date.now() - awardsTimestamp) > 1000 * 60 * MINUTES_TO_KEEP)) {
      setAwards([])
      setAwardsTimestamp(0)
    }
  }, [tick])

  useEffect(() => {
    let h = setInterval(() => setTick(Date.now()), 1000)
    return () => clearInterval(h)
  }, [])

  return (
    <div>
      <div style={{ textAlign: 'center', fontSize: '20px' }}>
	{awards}
      </div>
      <hr/>
    </div>    
  )
}
