import { useRef, useState, useEffect } from 'react';

export function Grid({ data, renderCell }) {
  return (
    <div
      className="canvas"
      style={{
	height: '100%',
	display: 'flex',
	flexDirection: 'column',
      }}
    >
      {data.map((row, gy) => (
	<Row key={gy}>
	  {row.map((data, gx) => (
	    <Cell key={gx} x={gx} y={gy} data={data} render={renderCell}/>
	  ))}
	</Row>
      ))}
    </div>
  )
}

function Cell({ render, data, x, y }) {
  return (
    <div
      style={{
	flexGrow: 1,
      }}
      className="cell gridlines"
    >
      {render({ data, x, y })}
    </div>
  )
}

function Row({ children }) {
  return (
    <div style={{
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'row',
    }}>
      {children}
    </div>
  )
}
