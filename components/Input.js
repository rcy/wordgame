import { useState, useEffect, useRef } from 'react';

export const Input = ({
  onSubmit = ()=>{},
  onChange = ()=>{},
  disabled,
  reset,
  middleware = ()=>{},
  commands = [],
  style,
  placeholder,
}) => {
  const [value, setValue] = useState('')
  const [lastKeyCode, setLastKeyCode] = useState(0)

  const textInput = useRef(null);

  useEffect(focus, [disabled])

  useEffect(() => {
    console.log('reset')
    setValue('')
    onChange('')
  }, [reset])

  function focus() {
    textInput.current.focus();
  }

  function keyDown(event) {
    if (event.keyCode === 13) {
      onSubmit(value)
    }
    if (event.keyCode === 27) {
      if (lastKeyCode === 27) {
        setValue('')
        onChange('')
      }
      onSubmit('')
    }
    setLastKeyCode(event.keyCode)
  }

  function change(ev) {
    const value = ev.target.value
    setValue(value);
    onChange(value);
  }

  return (
    <input
      type="text"
      disabled={disabled}
      onChange={change}
      onKeyDown={keyDown}
      value={value}
      ref={textInput}
      onBlur={focus}
      style={style}
      placeholder={placeholder}
    />
  )
}
