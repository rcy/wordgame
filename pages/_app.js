import Head from 'next/head'
import '../styles/globals.css'
import { Awards } from '../components'

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <Head>
	<title>Ruby's World</title>
	<script async defer data-domain="wordgame.ryanyeske.com" src="https://plausible.wordgame.ryanyeske.com/js/index.js"></script>
      </Head>
      <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
	<div>
	  <Awards />
	</div>
	<div style={{ flexGrow: 1 }}>
	  <Component {...pageProps} />
	</div>
      </div>
    </div>
  )
}

export default MyApp
