const { image_search } = require('duckduckgo-images-api');

export default async ({ query: { query } }, res) => {
  const options = { query: `cartoon ${query}`, moderate: true }
  const result = await image_search(options)
  res.statusCode = 200
  res.json(result)
}
