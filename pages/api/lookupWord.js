const words = require('./words.json')

function lookupWord(input) {
  const word = (input || "").trim().toLowerCase();
  let valid = false;
  let match;
  if (match = words.find(candidate => candidate && candidate.toLowerCase() === word)) {
    valid = true
  }
  return { match, valid, word, input }
}

export default async (req, res) => {
  const { word } = req.query
  try {
    const result = await lookupWord(word);
    res.statusCode = 200
    res.json(result)
  } catch(e) {
    console.error(e)
    res.statusCode = 500
    res.json({ error: true })
  }
}
