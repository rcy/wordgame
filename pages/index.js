import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import createPersistedState from 'use-persisted-state';
import { InputWithCommand as Input } from '../components' 
import { useRouter } from 'next/router'
import { useWordsHistoryState } from '../lib/persistence';

function split(text) {
  return text.trim().split(/\s+/)
}

const AppIndex = () => (
  <WordGame />
);
export default AppIndex

const maxQuota = 25
const breakSeconds = 10 * 60

const WordGame = () => {
  const router = useRouter()
  const [query, setQuery] = useState(null)
  const [quota, setQuota] = useState(0)
  const [history, setHistory] = useWordsHistoryState({})
  const [currentWord, setCurrentWord] = useState(null)

  function handleSubmit(value) {
    setQuery(value)
  }

  function handleSuccess({ query }) {
    updateHistory(query)
    setQuota(quota+1)
  }

  function updateHistory(query) {
    const words = query.trim().split(/\s+/)
    words.forEach(word => {
      history[word] = history[word] ? history[word] + 1 : 1;
    })
    setHistory(history)
  }

  function handleChange(value) {
    if (value.length > 0) {
      if (value[value.length - 1] !== ' ') {
        const words = split(value)
        setCurrentWord(words[words.length - 1])
      } else {
        setCurrentWord(null)
      }
    } else {
      setCurrentWord(null)
    }
  }

  return (
    <div className="container">
      <div className="column one">
      </div>
      <div className="column two">
        <h1>Ruby's World</h1>
        <div style={{ height: "50px" }}>
          {quota >= maxQuota ?
           <BreakTime
             seconds={breakSeconds}
             onDone={() => setQuota(0)}
           /> : <Quota quota={quota} maxQuota={maxQuota}/>
          }
        </div>

        <Input
	  onSubmit={handleSubmit}
	  disabled={quota >= maxQuota}
	  onChange={handleChange}
          placeholder="words or /command"
	/>

        {query && <Result query={query} onSuccess={handleSuccess} />}
      </div>
      <div className="column three">
        <HistoryList history={history} currentWord={currentWord} />
      </div>
    </div>
  )
}

function match(text, prefix) {
  if (text && text.length && prefix && prefix.length) {
    const match = text.substring(0, prefix.length)
    return match === prefix
  }
  return false
}

const HistoryList = ({ history, currentWord }) => {
  const entries = Object.entries(history).reverse().filter(entry => match(entry[0], currentWord))
  return (
    <ul>
      {entries.map(([word,count]) => (
        <li key={word}>{word}</li>
      ))}
    </ul>
  )
}

const BreakTime = ({ seconds, onDone }) => {
  const [start] = useState(Date.now())
  const [remaining, setRemaining] = useState(seconds)

  function tick() {
    const r = seconds - ((Date.now() - start) / 1000)
    setRemaining(Math.ceil(r))
    if (r <= 0) {
      onDone()
    } else {
      setTimeout(tick, 1000)
    }
  }

  useEffect(() => {
    tick()
  }, [])

  let interval
  if (remaining > 60) {
    interval = `${Math.ceil(remaining / 60)} minutes`
  } else {
    interval = `${remaining} seconds`
  }

  return <div className="break">⌚⌚⌚ break for {interval} ⌚⌚⌚</div>
}

const Quota = ({ quota, maxQuota }) => {
  return <progress value={quota} max={maxQuota}></progress>
}

const eqRe = /^(.+)([-+])(.+)=(.*)$/

function equationMatch(text) {
  const ctext = text.replace(/\s+/,'')
  //console.log({ text, ctext })
  return ctext.match(eqRe);
}

function evalEquation(text) {
  const match = equationMatch(text)
  if (!match) {
    throw new Error('not valid equation')
  }
  let result;
  const [_, s1, op, s2, s3] = match
  const [n1, n2, n3] = [s1, s2, s3].map(s => s ? +s : null)
  console.log({ n1, n2, n3})
  switch (op) {
    case '-': result = n1 - n2; break
    case '+': result = n1 + n2; break
    default: throw new Error ('invalid op')
  }
  
  console.log({ result })
  if (n3) {
    if (n3 === result) {
      return true
    } else {
      return false
    }
  } else {
    return result
  }
}

const Result = ({ query, onSuccess }) => {
  const [tokens, setTokens] = useState([])
  const [allValid, setAllValid] = useState(false)
  const [mathResult, setMathResult] = useState(null)

  useEffect(() => {
    if (query) {
      setTokens([])
      setAllValid(false)
      if (equationMatch(query)) {
	const result = evalEquation(query)
	if (typeof result === 'number') {
	  setMathResult(result)
	} else {
	  setMathResult(result ? 'yes' : 'no');
	}
      } else {
	const words = query.trim().split(/\s+/)
	setMathResult(null)
	Promise.all(words.map(word => lookupWord(word))).then((tokens) => {
          console.log({ tokens })
          setTokens(tokens)
          const allValid = !tokens.find(token => !token.valid)
          setAllValid(allValid)

          playSound({ kind: allValid ? 'positive' : 'negative' })

          if (allValid) {
            onSuccess({ query })
          }
	})
      }
    }
  }, [query])

  function handleImageSuccess() {
    console.log('image success handler')
  }

  return (
    <div>
      <div style={{ margin: "0 0 .5em 0"}}>
        <Tokens tokens={tokens} />
      </div>
      {allValid ? <QueryImage query={query} onSuccess={handleImageSuccess} /> : null}
      {mathResult !== null ? (
	<h1 className="math-result">{mathResult}</h1>
      ) : null}
    </div>
  )
}

const QueryImage = ({ query, onSuccess }) => {
  const [image, setImage] = useState()
  const [loading, setLoading] = useState()

  useEffect(() => {
    let cancelled = false

    setImage()
    setLoading(true)

    findImage(query)
      .then(result => {
        if (cancelled) {
          return
        }
        setLoading(false)
        console.log(result)
        if (result) {
          setImage(result.thumbnail)
          onSuccess()
        }
      })

    return () => cancelled = true;
  }, [query])

  if (loading) {
    return "..."
  }

  return (
    <div>
      {image ? <img src={image} /> : null}
    </div>
  )
}

const Tokens = ({tokens}) => {
  return (
    <div>
      {tokens.map((token) => (
        <Token key={token.word} token={token} />
      ))}
    </div>
  )
}

const Token = ({ token }) => {
  return (
    <>
      <span
        style={{
          backgroundColor: token.valid ? 'limegreen' : 'red'
        }}
      >
        {token.word}
      </span>
      &nbsp;
    </>
  )
}

async function findImage(query) {
  const url = new URL(window.location.href)
  url.pathname = "/api/findImage"
  url.search = new URLSearchParams({ query })
  const result = await fetch(url)
  const json = await result.json()
  return json[0]
}

async function lookupWord(word) {
  const url = new URL(window.location.href)
  url.pathname = "/api/lookupWord"
  url.search = new URLSearchParams({ word })
  const result = await fetch(url)
  return await result.json()
}

function playSound({ kind }) {
  return new Promise((resolve, reject) => {
    let file

    if (kind === 'negative') {
      file = randomElement([
        '/sounds/negative/negative-dad.ogg',
        '/sounds/negative/negative-ruby-2.ogg',
        '/sounds/negative/negative-ruby-4.ogg',
        '/sounds/negative/negative-ruby.ogg',
      ])
    } else if (kind === 'positive') {
      file = randomElement([
        '/sounds/positive/positive-dad.ogg',
        '/sounds/positive/positive-ruby-3.ogg',
        '/sounds/positive/positive-ruby.ogg',
      ])
    } else {
      reject(new Error('kind not negative or positive'))
      return
    }

    console.log({ file })

    const audio = new Audio(file)
    audio.addEventListener('ended', (event) => {
      console.log('audio ended')
      resolve()
    })
    audio.play()
  })
}

function randomElement(arr) {
  return arr[Math.floor(Math.random() * arr.length)]
}
