import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import createPersistedState from 'use-persisted-state';
import { InputWithCommand as Input } from '../components'
import { tests } from '../lib/makeTest'
import { useRouter } from 'next/router'
import { shuffle } from '../lib/shuffle'

const Quiz = () => {
  const router = useRouter()
  const [test, setTest] = useState(null)

  useEffect(() => {
    const testName = router.query.test
    const theTest = tests.find(t => t.name === testName)

    if (theTest) {
      setTest({
	...theTest,
	questions: shuffle(theTest.questions).slice(0,10),
      })
    }
  }, [router.query])

  return (
    <>
      {test && <QuizBody test={test} />}
    </>
  )
}

function QuizBody({ test }) {
  const [qi, setQi] = useState(0)
  const [wait, setWait] = useState(false)
  const [question, setQuestion] = useState(test.questions[0])
  const [perfect, setPerfect] = useState(true)
  const router = useRouter()

  function handleCorrect({ tries }) {
    if (tries >= 5) {
      setPerfect(false)
    }
    if (qi >= test.questions.length - 1) {
      router.push({
	pathname: '/confetti',
	query: {
          test: test.name,
          perfect,
        },
      })
      return
    }

    setWait(true)
    setTimeout(() => {
      setQi(() => qi + 1)
      setWait(() => false)
    }, 1000)
  }

  useEffect(() => {
    setQi(0)
    setWait(false)
  }, [test])

  useEffect(() => {
    if (test) {
      setQuestion(test.questions[qi])
    }
  }, [qi, test])

  return (
    <>
      <header style={{ textAlign: 'center', color: test.color }} >
	<h2 style={{ marginBottom: 0 }}>
	  {test.title}
	</h2>
	<p style={{ marginTop: 0 }}>
	  {test.description}
	</p>
      </header>

      <div style={{ color: 'gray', fontSize: '.5em', float: 'right' }}>
	{qi + 1}/{test.questions.length}
      </div>
      {question &&
       <Question
	 qa={question}
	 onCorrect={handleCorrect}
	 wait={wait}
	 emoji={test.emoji}
       />}
    </>
  )
}

function Question({qa, onCorrect, wait, emoji}) {
  const [input, setInput] = useState()
  const [feedback, setFeedback] = useState({})
  const [reset, setReset] = useState(Date.now())
  const [wrongCount, setWrongCount] = useState(0)
  const [tries, setTries] = useState(1)
  const router = useRouter()

  useEffect(() => {
    setFeedback({})
    setReset(() => Date.now())
  }, [qa.id])

  function handleSubmit(x) {
    if (x) {
      setTries(() => tries + 1)
      if (x.toLowerCase() === qa.a.toLowerCase()) {
	setFeedback({ text: `${emoji} Correct!`, color: 'green' })
	setWrongCount(0)
	onCorrect({ tries })
        setTries(1)
      } else {
	if (wrongCount >= 3) {
	  setFeedback({ text: `try ${qa.a}`, color: 'orange' })
	  setWrongCount(0)
	} else {
	  setWrongCount(() => wrongCount + 1)
	  setFeedback({
	    color: 'red',
	    text: [
	      '🙈 nope, try again!',
	      '🙈🙉 not quite',
	      '🙈🙉🙊 keep trying!'
	    ][wrongCount]
	  })
	}
      }
    }
  }

  function handleChange() {
    setFeedback({})
  }

  return (
    <>
      <div>{qa.q}</div>
      <Input
        onSubmit={handleSubmit}
	onChange={handleChange}
	reset={reset}
	disabled={wait}
	placeholder="answer or /command"
      />
      <div style={{ color: feedback.color }}>{feedback.text}</div>
    </>
  )
}
    
export default Quiz
