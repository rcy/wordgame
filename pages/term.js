import { useState, useEffect, useRef, useReducer } from 'react';

const initialState = { elements: [], program: 'root' }

function reducer(state, action) {
  switch (action.type) {
    case 'input':
      return { ...state, elements: state.elements.concat({ type: 'input', value: action.value, color: action.color }) }
    case 'output':
      return { ...state, elements: state.elements.concat({ type: 'output', value: action.value, color: action.color }) }
    case 'error':
      return { ...state, elements: state.elements.concat({ type: 'error', value: action.value, color: action.color }) }
    case 'clear':
      return { ...state, elements: [] }
    case 'load':
      if (!programs[action.program]) {
        return { ...state, elements: state.elements.concat({ type: 'error', value: `load: program ${action.program} not found` }) }
      }
      return { ...state, program: action.program }
    case 'exit':
      return { ...state, program: 'root' }
    default:
      throw new Error()
  }
}

function noop() {}

function c_help(dispatch, _arga, program) {
  dispatch({ type: 'output', value: Object.keys(program.commands).join(',') })
}

function c_clear(dispatch) {
  dispatch({ type: 'clear' })
}

function c_echo(dispatch, arga) {
  dispatch({ type: 'output', value: arga.join(' ') })
}

function c_alias(dispatch, arga, program) {
  const name = arga[0]
  const definition = arga.slice(1).join(' ')

  dispatch({ type: 'output', value: `ALIAS ${name} ${definition}` })

  if (definition) {
    program.commands[name] = function(dispatch, arga) {
      dispatch({ type: 'output', color: 'gray', value: `: ${definition}` })
      exec(program, dispatch, `${definition} ${arga.join(' ')}`)
    }
  } else {
    delete program.commands[name]
  }
}

const programs = {
  root: {
    prompt: '>',
    commands: {
      'alias': c_alias,
      'clear': c_clear,
      'echo': c_echo,
      'help': c_help,
    }
  },
  pink: {
    prompt: 'pink>',
    commands: {
      'pink': (dispatch) => dispatch({ type: 'output', color: 'pink', value: 'pink pink pink' })
    }
  }
}

function exec(program, dispatch, input) {
  const tokens = input.split(/\s+/)
  const name = tokens[0]
  const arga = tokens.slice(1) // arg array
  const cmd = program.commands[name]
  
  if (cmd) {
    cmd(dispatch, arga, program)
  } else {
    dispatch({ type: 'error', value: `${name}: command not found` })
  }
}

const Term = () => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const program = programs[state.program]

  function handleInput(input) {
    dispatch({type: 'input', value: `${program.prompt} ${input}`})

    const cmd = input.split(/\s+/)[0]
    if (cmd === 'load') {
      const programName = input.split(/\s+/)[1] 
      dispatch({ type: 'load', program: programName })
    } else if (cmd === 'exit') {
      dispatch({ type: 'exit' })
    } else {
      exec(program, dispatch, input)
    }
  }

  return (
    <Layout>
      <TerminalOutput elements={state.elements} />
      <TerminalInput prompt={program.prompt} onSubmit={handleInput} />
    </Layout>
  )
}

function Layout({ children }) {
  return (
    <div
      className="terminal"
      style={{ height: '100%' }}
    >
      {children}
    </div>
  )
}

function TerminalInput({ prompt, onSubmit }) {
  return (
    <>
      <Prompt prompt={prompt} />
      <Input onSubmit={onSubmit} />
    </>
  )
}

function Prompt({ prompt }) {
  return `${prompt} `
}

function Input({ onChange, onSubmit }) {
  const [input, setInput] = useState('')

  function handleKey(e) {
    if (e.keyCode === 13) {
      onSubmit(input)
      setInput('')
    }
  }

  return (
    <input
      type='text'
      onChange={e => setInput(e.target.value)}
      onKeyDown={handleKey}
      value={input}
    />
  )
}

function TerminalOutput({ elements }) {
  return (
    <div className="output">
      {elements.map(elem => (
        <div
          className={elem.type}
          style={{ color: elem.color }}
        >
          {elem.value}
        </div>
      ))}
    </div>
  )
}

export default Term
