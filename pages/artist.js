import { useRef, useState, useEffect } from 'react';
import { Grid } from '../components';
import { useRouter } from 'next/router';

// https://sashamaps.net/docs/resources/20-colors/
const colors = [
  '#000000',
  '#e6194B',
  '#f58231',
  '#ffe119',
  '#bfef45',
  '#3cb44b',
  '#42d4f4',
  '#4363d8',
  '#911eb4',
  '#f032e6',
]

function makeGrid(rows, columns) {
  const grid = []

  for (let y = 0; y < rows; y++) {
    grid[y] = []
    for (let x = 0; x < columns; x++) {
      grid[y][x] = {
	color: ``,
      }
    }
  }

  return grid
}

const X_SIZE = 15
const Y_SIZE = 15

const Draw = () => {
  const router = useRouter()
  const [x, setX] = useState(Math.floor(X_SIZE / 2))
  const [y, setY] = useState(Math.floor(Y_SIZE / 2))
  const [color, setColor] = useState(colors[1])
  
  const [gridData, setGridData] = useState([])

  const gridContainerRef = useRef()
  
  useEffect(() => {
    setGridData(makeGrid(X_SIZE, Y_SIZE))
  }, [])

  function keyup(ev) {
    ev.preventDefault()

    switch (ev.key) {
      case 'Escape':
	router.push('/')
	break
      case 'ArrowDown':
	setY((y) => Math.min(y + 1, Y_SIZE - 1))
	break;
      case 'ArrowUp':
	setY((y) => Math.max(y - 1, 0))
	break;
      case 'ArrowLeft':
	setX((x) => Math.max(x - 1, 0))
	break;
      case 'ArrowRight':
	setX((x) => Math.min(x + 1, X_SIZE -1))
	break;
      case ' ':
	console.log('space', x, y)
	if (gridData[y][x].color === color) {
	  gridData[y][x].color = ''
	} else {
	  gridData[y][x].color = color
	}
	setGridData([])
	setGridData(gridData)
	break;
      case '0':
	setColor(color => colors[0])
	break
      case '1':
	setColor(color => colors[1])
	break
      case '2':
	setColor(color => colors[2])
	break
      case '3':
	setColor(color => colors[3])
	break
      case '4':
	setColor(color => colors[4])
	break
      case '5':
	setColor(color => colors[5])
	break
      case '6':
	setColor(color => colors[6])
	break
      case '7':
	setColor(color => colors[7])
	break
      case '8':
	setColor(color => colors[8])
	break
      case '9':
	setColor(color => colors[9])
	break
      case 'C':
        for (let x = 0; x < X_SIZE; x++) {
          for (let y = 0; y < Y_SIZE; y++) {
            console.log({ x, y })
            gridData[y][x].color = color
          }
        }
        setGridData([])
        setGridData(gridData)
        break
      default:
	console.log('keyx', ev.key, ev.keyCode)
    }
  }

  useEffect(() => {
    const EVENT = 'keyup'
    document.addEventListener(EVENT, keyup)
    return () => document.removeEventListener(EVENT, keyup)
  }, [x, y, color])

  return (
    <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }} >
      <h1 style={{ color: '#222', background: color }}>artist {x},{y}</h1>
      <div
	ref={gridContainerRef}
	style={{
	  flexGrow: 1,
	  border: '10px solid #ccc',
	  background: '#eee',
	}}
      >
	<Grid
	  data={gridData}
	  renderCell={({ data, x: gx, y: gy }) => (
	    <div
	      style={{
		height: '100%',
		background: gridData[gy][gx].color,
	      }}
	    >
	      <div className={(x == gx && y == gy) ? 'current' : null} />
	    </div>
	  )}
	/>
      </div>
    </div>
  )
}
export default Draw;
