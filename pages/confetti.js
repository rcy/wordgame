import { useEffect, useState } from 'react';
import { InputWithCommand as Input } from "../components";
import { useWindowSize } from 'react-use';
import Confetti from 'react-confetti'
import { useRouter } from 'next/router'
import { tests } from '../lib/makeTest'
import { useAwardsHistoryState, useAwardsTimestampHistoryState } from '../lib/persistence'

export default function ConfettiPage() {
  const router = useRouter()
  const [test, setTest] = useState()
  const [awards, setAwards] = useAwardsHistoryState([])
  const [awardsTimestamp, setAwardsTimestamp] = useAwardsTimestampHistoryState([])
  const { width, height } = useWindowSize()
  const [perfect, setPerfect] = useState()

  useEffect(() => {
    setTest(tests.find(t => t.name === router.query.test))
    setPerfect(router.query.perfect === 'true')
  }, [router.query])

  useEffect(() => {
    if (test && perfect) {
      // only award emoji if not already earned
      if (awards.indexOf(test.emoji) === -1) {
        setAwards(() => [...awards, test.emoji])
        setAwardsTimestamp(Date.now())
      }
    }
  }, [test])

  if (!test) {
    return null
  }

  return (
    <div>
      {perfect &&
       <Confetti
         width={width}
         height={height}
         colors={[ test.color ]}
       />}
      {perfect &&
       <div
	 style={{
	   textAlign: 'center',
	   marginTop: '0px',
	   fontSize: '200px',
	 }}
       >
	 {test.emoji}
       </div>}

      <div style={{ textAlign: 'center' }}>
        {perfect ? <h2>You completed</h2> : <h2>🙈🙉🙊 Try this one again! 🙈🙉🙊</h2>}
	<h1 style={{ color: test.color }}>{test.title}</h1>
      </div>

      <br/>

      <Input placeholder="Press / for commands"/>
    </div>
  )
}
